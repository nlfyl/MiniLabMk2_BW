{
  inputs = { nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable"; };

  outputs = { self, nixpkgs }:
    let pkgs = nixpkgs.legacyPackages.x86_64-linux;
    in
    {
      devShell.x86_64-linux = pkgs.mkShell
        {
          buildInputs =
            [
              pkgs.maven
              pkgs.jdk11
            ];
          nativeBuildInputs =
            [
            ];
          propagatedBuildInputs =
            [
            ];
            #[
              #ladspaH ladspa-sdk lilv lv2 suil expat inih waf libvterm
              #libtool inotify-tools libmpc mpfr gmp gawk bison flex texinfo
              #patchutils linuxKernel.packages.linux_5_10.perf perf-tools libyamlcpp
              #xorg.libX11.dev xorg.libXxf86vm xorg.libXft xorg.libXinerama alsa-lib
              #cairo freetype gdk-pixbuf glib gtk3 xorg.libxcb xorg.xcbutil
              #xorg.xcbutilwm xorg.libXtst libxkbcommon pulseaudio libjack2 xorg.libX11
              #libglvnd xorg.libXcursor readline libtool libunistring
              #libffi boehmgc stm32flash stm32loader gcc-arm-embedded stlink stlink-gui
              #gst_all_1.gstreamer extra-cmake-modules pkgs.qt5.full libsForQt5.plasma-framework
              #libsForQt5.kirigami2 wxGTK31 xorg.libX11 pipewire taglib libyamlcpp
              #libsndfile spdlog speex libinput xorg.libxcb libxkbcommon pixman
            #]

          shellHook =
            ''
              echo "meowW"
              export CMAKE_GENERATOR=Ninja
            '';
        };
   };
}

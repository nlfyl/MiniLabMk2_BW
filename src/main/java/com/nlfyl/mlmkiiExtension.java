package com.nlfyl;

import com.bitwig.extension.api.util.midi.ShortMidiMessage;
import com.bitwig.extension.callback.ShortMidiMessageReceivedCallback;
import com.bitwig.extension.controller.ControllerExtension;
import com.bitwig.extension.controller.api.*;

import java.util.Arrays;

//private class mlmk2
//{
//}

public class mlmkiiExtension extends ControllerExtension
{
    public SysexSend sending;
    public Application app;

    NoteInput MiniLabPads;
    Integer[] padTranslation = new Integer[128];

    public TimelineEditor tl;
    public Arranger arrngr;

    public Transport transport;
    public CursorTrack track;
    public PinnableCursorDevice device;
    public CursorRemoteControlsPage macros;
    //freel asssignable knobs
    public UserControlBank ccKnobs;
    // midi messages.
    public static final int MIDI_STATUS_PAD_ON = 153;
    public static final int MIDI_STATUS_PAD_OFF = 137;
    public static final int MIDI_STATUS_KNOBS = 176;
    public static final int MIDI_DATA1_PAD_OFFSET = 36;

    // knobs mapping (midi data1).
    public static final int KNOB1_SHIFT = 7;
    public static final int KNOB1_CLICK = 113;
    public static final int KNOB16 = 72;
    public static final int KNOB5 = 85;
    public static final int KNOB9_SHIFT = 116;
    public static final int KNOB9_CLICK = 115;
    //public static final int knobs_assignable_ = [77, 93, 73, 17, 91, 79, 72];
    //public static final int knobs_macros = [112, 74, 71, 76, 114, 18, 19, 16];

    public static final Integer[] KNOBS_ASSIGNABLE = { 93, 73, 75, 17, 91, 79, 72};
    public static final Integer[] KNOBS_MACROS = {112, 74, 71, 76, 114, 18, 19, 16};

    // minilab colors.
    public static final String[] COLOR =
    {
        "00", //black
        "01", //red
        "10", //blue
        "04", //green
        "14", //cyan
        "11", //purple
        "05", //yellow
        "7f" //white
    };

    public static final String[] PAD_COLORS =
    {
        "00",
        "00",
        "00",
        "00",

        "00",
        "00",
        "00",
        "00",

        "00",
        "00",
        "00",
        "00",

        "00",
        "00",
        "00",
        "00"
    };
    public static final String[] KNOB1_ALT_PAD_COLORS =
    {
        "00",
        "11",
        "14",
        "7f",
        "7f",
        "10",
        "05",
        "01",

        "05",
        "01",
        "10",
        "14",
        "14",
        "10",
        "01",
        "05"
    };

    protected
    mlmkiiExtension(final mlmkiiExtensionDefinition definition,
                    final ControllerHost host)
    {
        super(definition, host);
    }

    //private void
    //setPadColor(int pad, String[] color)
    //{
    //int padHexInt = (112 + pad);
    //String padHex = Integer.toHexString(padHexInt);
    //midiOut.sendSysex("F0 00 20 6B 7F 42 02 00 10 " + padHex + " " + color +" F7");
    //}

    //TODO move this shit back to init when you can
    @Override
    public void
    init()
    {
        final ControllerHost host = getHost();
        sending = new SysexSend(host);

        //NOTE: start of the conversion from js init
        final NoteInput MiniLabKeys = host.getMidiInPort(
                0).createNoteInput("MiniLab Keys", "80????", "90????", "B001??", "B002??",
                                      "B007??", "B00B??", "B040??", "C0????", "D0????", "E0????");
        MiniLabKeys.setShouldConsumeEvents(false);

        MiniLabPads = host.getMidiInPort(
                                  0).createNoteInput("MiniLab Pads", "?9????");
        MiniLabPads.setShouldConsumeEvents(false);
        MiniLabPads.assignPolyphonicAftertouchToExpression(0,
                NoteInput.NoteExpression.TIMBRE_UP, 2);


        /// Here we set the pad table, it supports up to 127 pads by the look of it
        /// when we initiate it here, we are basically zeroing out the array
        /// then on 2 we are basically telling bitwig how many pads we have available and their corresponding midi number
        /// as you can see by the '+ 16' we are telling it that we are gonna select the first 16 pads
        /// in this case we ONLY have 16 so we are enabling all of them
        /// on 3 we are finally sending that information through the api and setting the colors
        /// 1
        for (int i = 0; i < 128; i++)
        {
            padTranslation[i] = -1;
        }
        /// 2
        for (int i = MIDI_DATA1_PAD_OFFSET; i < MIDI_DATA1_PAD_OFFSET + 16; i++)
        {
            padTranslation[i] = i;
        }
        /// 3
        MiniLabPads.setKeyTranslationTable(padTranslation);
        updatePads(PAD_COLORS);

        app = host.createApplication();
        transport = host.createTransport();
        track = host.createCursorTrack(1, 1);
        device = track.createCursorDevice();
        macros = device.createCursorRemoteControlsPage(8);
        arrngr = host.createArranger();
        //freel asssignable knobs
        ccKnobs = host.createUserControls(6);

        host.getMidiInPort(0).setMidiCallback((ShortMidiMessageReceivedCallback)msg ->
                                              onMidi0(msg));
        host.getMidiInPort(0).setSysexCallback((String data) -> onSysex0(data));

        host.showPopupNotification("MiniLab Mk2 Initialized");
    }

    @Override
    public void
    exit()
    {
        // TODO: Perform any cleanup once the driver exits
        // For now just show a popup notification for verification that it is no longer running.
        getHost().showPopupNotification("MiniLab Mk2 Exited");
    }

    @Override
    public void
    flush()
    {
        // TODO Send any updates you need here.
    }

    void
    updatePads(String[] padSet)
    {
        for (int i = 0; i < 16; i++)
        {
            sending.setPadColor(i, padSet[i]);
        }

    }

    boolean alt_knob1 = false;
    boolean alt_knob9 = false;
    long st_t = 0;
    long end_t = 0;
    double tot = 0;

    int knob5_slow_pos = 0;
    int knob5_slow_neg = 0;
    /** Called when we receive short MIDI message on port 0. */
    // Called when a short MIDI message is received on MIDI input port 0.
    // Status: Note on/off, CC, PB, ...
    // Data1: Note number, CC number
    // Data2: Note velocity, pressure, CC data (0-127)
    private void
    onMidi0(ShortMidiMessage msg)
    {
        // TODO: Implement your MIDI input handling code here.
        int padNumber = msg.getData1() - MIDI_DATA1_PAD_OFFSET;
        int status = msg.getStatusByte();

        /// MIDI_STATUS_PAD_ON is sent when we initially press the pad
        /// Here we check if we are currently in our alternative mode so we can properly use the utility
        /// assigned to the pads
        if (status == MIDI_STATUS_PAD_ON && alt_knob1 == true)
        {
            switch (padNumber)
            {
                case 0:
                    track.mute().toggle();
                    break;
                case 1:
                    track.solo().toggle();
                    break;
                case 2:
                    transport.isArrangerLoopEnabled().toggle();
                    break;
                case 3:
                    track.selectNext();
                    break;
                case 4:
                    track.selectPrevious();
                    break;
                case 5:
                    transport.stop();
                    break;
                case 6:
                    transport.togglePlay();
                    break;
                case 7:
                    transport.record();
                    break;

                case 8:
                    app.previousSubPanel();
                    break;
                case 9:
                    app.undo();
                    break;
                case 10:
                    macros.selectPreviousPage(true);
                    break;
                case 11:
                    device.selectPrevious();
                    break;
                case 12:
                    device.selectNext();
                    break;
                case 13:
                    macros.selectNextPage(true);
                    break;
                case 14:
                    app.redo();
                    break;
                case 15:
                    app.nextSubPanel();
                    break;
            }
        }
        /// MIDI_STATUS_PAD_OFF is sent when we lift the finger off of the pad
        /// For some reason that im too lazy to figure out sometimes when changing pages
        /// pad colors dont get updated so its better to refresh it everytime we lift the finger off
        /// PERF: could potentially make it so this is done ONCE when we are in alt_knob1 mode and thats it
        else if (status == MIDI_STATUS_PAD_OFF && alt_knob1 == true)
        {
//            sending.setPadColor(padNumber, KNOB1_ALT_PAD_COLORS[padNumber]);
            updatePads(KNOB1_ALT_PAD_COLORS);
        }
        else if (status == MIDI_STATUS_KNOBS)
        {
            int increment = msg.getData2() - 64;
            switch (msg.getData1())
            {
                case KNOB1_SHIFT:
                    if (Integer.signum(increment) == 1)
                    {
                        arrngr.zoomIn();
                        transport.rewind();
                        transport.fastForward();
                    }
                    else if (Integer.signum(increment) == -1)
                    {
                        arrngr.zoomOut();
                        transport.rewind();
                        transport.fastForward();
                    }
                    break;
                case KNOB1_CLICK:
                    {
                        if (63 == increment)
                        {
                            st_t = System.nanoTime();
                        }
                        if (-64 == increment)
                        {
                            end_t = System.nanoTime();
                            tot = (end_t - st_t) / 1000000;
                            /// When only works when both modes are off
                            ///when we are in default
                            /// This 600 is milliseconds (i think lol)
                            if (tot > 600 && alt_knob1 == false && alt_knob9 == false)
                            {
                                alt_knob1 = true;
                                /// NOTE: Here we zero out the padTranslation table so we basically have no pads enabled
                                for (int i = 0; i < 128; i++)
                                {
                                    padTranslation[i] = -1;
                                }
                                MiniLabPads.setKeyTranslationTable(padTranslation);
                                updatePads(KNOB1_ALT_PAD_COLORS);
                                getHost().showPopupNotification("MiniLab Mk2: Utility Mode On");
                            }
                            /// Can only disable alt mode for knob 1 when its in alt mode for knob 1 duh
                            /// and not alt mode for knob 9
                            else if (tot > 600 && alt_knob1 == true && alt_knob9 == false)
                            {
                                alt_knob1 = false;
                                /// NOTE: Here we do the same thing we did in the beginning and zero out the table
                                /// followed by assigning the first 16 pads to their respective midi number
                                /// so they can be used as midi input/ note input :}
                                for (int i = 0; i < 128; i++)
                                {
                                    padTranslation[i] = -1;
                                }
                                for (int i = MIDI_DATA1_PAD_OFFSET; i < MIDI_DATA1_PAD_OFFSET + 16; i++)
                                {
                                    padTranslation[i] = i;
                                }
                                MiniLabPads.setKeyTranslationTable(padTranslation);
                                updatePads(PAD_COLORS);
                                getHost().showPopupNotification("MiniLab Mk2: Utility Mode Off");

                            }
                            ///This only works if we are in altmode for knob 1 and alt mode for knob 9 is off
                            else if (tot > 600 && alt_knob1 == false && alt_knob9 == true)
                            {
                                getHost().showPopupNotification("This feature is not enabled yet");
                            }
                            else
                            {
                                device.isEnabled().toggle();
                            }
                        }
                    }
                    break;
                case KNOB16:
                    track.volume().inc(increment, 128);
                    break;
                case KNOB9_SHIFT:
                    track.volume().reset();
                    break;
                case KNOB9_CLICK:
                    {
                        if (63 == increment)
                        {
                            st_t = System.nanoTime();
                        }
                        if (-64 == increment)
                        {
                            end_t = System.nanoTime();
                            tot = (end_t - st_t) / 1000000;
                            /// When only works when both modes are off
                            ///when we are in default
                            if (tot > 600 && alt_knob9 == false && alt_knob1 == false)
                            {
                                alt_knob9 = true;
                                getHost().showPopupNotification("MiniLab Mk2: Sequencer not enabled yet.");
                                alt_knob9 = false;
                            }
                            /// Can only disable alt mode for knob 9 when its in alt mode for knob 9 duh
                            else if (tot > 600 && alt_knob9 == true && alt_knob1 == false)
                            {
                                alt_knob9 = false;
                                getHost().showPopupNotification("MiniLab Mk2: Sequencer off");
                            }
                            ///This only works if we are in altmode for knob 1 and alt mode for knob 9 is off
                            else if (tot > 600 && alt_knob9 == false && alt_knob1 == true)
                            {
                                device.isRemoteControlsSectionVisible().toggle();
                            }
                            else
                            {
                                device.isWindowOpen().toggle();
                            }
                        }
                    }
                    break;
                case KNOB5:
                    {
                        /// As it was, the transport moved too fast
                        /// This is so we have to spin it a certain amount before it moves
                        /// Making it feel a bit more organic
                        if (increment > 0)
                        {
                            knob5_slow_pos += increment;
                            if (knob5_slow_pos >= 8)
                            {
                                transport.fastForward();
                                knob5_slow_pos = 0;
                                knob5_slow_neg = 0;
                            }
                        }
                        if (increment < 0)
                        {
                            knob5_slow_neg += increment;
                            if (knob5_slow_neg <= -8)
                            {
                                transport.rewind();
                                knob5_slow_neg = 0;
                                knob5_slow_pos = 0;
                            }
                        }
                    }
                    break;
                default:
                    {
                        /// Here it looks to see if we are using assignable knobs
                        /// if we are it proceeds as programmed by the user
                        /// if we are using one of the knobs set by the script (1 - 4, 9 to 12)
                        /// it will modify macros (remote control pages)
                        int toFind = msg.getData1();
                        int ccIndex = Arrays.asList(KNOBS_ASSIGNABLE).indexOf(msg.getData1());
                        if (ccIndex > -1)
                        {
                            ccKnobs.getControl(ccIndex).inc(increment, 128);
                        }
                        /// Added for the mod wheel
                        else if (toFind == 1)
                        {
                            ccKnobs.getControl(toFind).set(msg.getData2(), 128);
                        }
                        else
                        {
                            int macroIndex = Arrays.asList(KNOBS_MACROS).indexOf(toFind);
                            if (Integer.signum(increment) == 1)
                            {
                                macros.getParameter(macroIndex).inc(1, 128);
                            }
                            if (Integer.signum(increment) == -1)
                            {
                                macros.getParameter(macroIndex).inc(-1, 128);
                            }
                        }
                    }
            }
        }
    }

    /** Called when we receive sysex MIDI message on port 0. */
    private void
    onSysex0(final String data)
    {
        // MMC Transport Controls:
        if (data.equals("f07f7f0605f7"))
            mTransport.rewind();
        else if (data.equals("f07f7f0604f7"))
            mTransport.fastForward();
        else if (data.equals("f07f7f0601f7"))
            mTransport.stop();
        else if (data.equals("f07f7f0602f7"))
            mTransport.play();
        else if (data.equals("f07f7f0606f7"))
            mTransport.record();
    }

    private Transport mTransport;
}

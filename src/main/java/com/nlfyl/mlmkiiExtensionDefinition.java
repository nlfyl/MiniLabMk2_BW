package com.nlfyl;
import java.util.UUID;

import com.bitwig.extension.api.PlatformType;
import com.bitwig.extension.controller.AutoDetectionMidiPortNamesList;
import com.bitwig.extension.controller.ControllerExtensionDefinition;
import com.bitwig.extension.controller.api.ControllerHost;

public class mlmkiiExtensionDefinition extends ControllerExtensionDefinition
{
    private static final UUID DRIVER_ID =
                UUID.fromString("2fdbbb1f-f771-43f7-83e8-bc01c3e573de");

    public
    mlmkiiExtensionDefinition()
    {
    }

    @Override
    public String
    getName()
    {
        return "MiniLab Mk2";
    }

    @Override
    public String
    getAuthor()
    {
        return "crimson99";
    }

    @Override
    public String
    getVersion()
    {
        return "1.5";
    }

    @Override
    public UUID
    getId()
    {
        return DRIVER_ID;
    }

    @Override
    public String
    getHardwareVendor()
    {
        return "Arturia";
    }

    @Override
    public String
    getHardwareModel()
    {
        return "MiniLab Mk2";
    }

    @Override
    public int
    getRequiredAPIVersion()
    {
        return 14;
    }

    @Override
    public int
    getNumMidiInPorts()
    {
        return 1;
    }

    @Override
    public int
    getNumMidiOutPorts()
    {
        return 1;
    }

    @Override
    public void
    listAutoDetectionMidiPortNames(final AutoDetectionMidiPortNamesList list,
                                   final PlatformType platformType)
    {
        if (platformType == PlatformType.WINDOWS)
        {
            // TODO: Set the correct names of the ports for auto detection on Windows platform here
            // and uncomment this when port names are correct.
            list.add(new String[] {"Arturia MiniLab mkII"}, new String[] {"Arturia MiniLab mkII"});
        }
        else if (platformType == PlatformType.MAC)
        {
            // TODO: Set the correct names of the ports for auto detection on Windows platform here
            // and uncomment this when port names are correct.
            list.add(new String[] {"Arturia MiniLab mkII"}, new String[] {"Arturia MiniLab mkII"});
        }
        else if (platformType == PlatformType.LINUX)
        {
            // TODO: Set the correct names of the ports for auto detection on Windows platform here
            // and uncomment this when port names are correct.
            list.add(new String[] {"Arturia MiniLab mkII"}, new String[] {"Arturia MiniLab mkII"});
        }
    }

    @Override
    public mlmkiiExtension
    createInstance(final ControllerHost host)
    {
        return new mlmkiiExtension(this, host);
    }
}

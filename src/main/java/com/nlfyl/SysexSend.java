package com.nlfyl;

import com.bitwig.extension.controller.api.ControllerHost;
import com.bitwig.extension.controller.api.MidiOut;

import java.util.ArrayList;
import java.util.List;

public class SysexSend
{

    private MidiOut port;


    public
    SysexSend(final ControllerHost host)
    {
        this(host, 0);
    }

    public
    SysexSend(final ControllerHost host, final int portNumber)
    {
        this.port = host.getMidiOutPort(portNumber);
    }

    public void
    sendsex(String message)
    {
        this.port.sendSysex(message);
    }

    public void
    setPadColor(int pad, String color)
    {
        int padHexInt = (112 + pad);
        String padHex = Integer.toHexString(padHexInt);
        this.sendsex("F0 00 20 6B 7F 42 02 00 10 " + padHex + " " + color +
                     " F7");
    }
}
